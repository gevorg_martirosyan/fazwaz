<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//  Route group for projects routes.
Route::group(['prefix' => '/projects'], function () {
    // Get projects with pagination and sorting.
    Route::get('/', 'Api\ProjectController@show');
});
