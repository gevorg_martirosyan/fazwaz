<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Country;
use App\Models\Region;
use Faker\Generator as Faker;

$factory->define(Country::class, function (Faker $faker) {
    return [
        'country'   =>  $faker->country,
        'region_id' =>  factory(Region::class)->create()->id,
    ];
});
