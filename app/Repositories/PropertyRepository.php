<?php


namespace App\Repositories;

use App\Contracts\PropertyInterface;
use App\Models\Property;

/**
 * Class PropertyRepository
 *
 * @package App\Repositories
 */
class PropertyRepository implements PropertyInterface
{
    /**
     * @var Property
     */
    protected $model;

    /**
     * PropertyRepository constructor.
     *
     * @param Property $model
     */
    public function __construct(Property $model)
    {
        $this->model = $model;
    }

    /**
     * Get paginated projects.
     *
     * @param $data
     * @return mixed
     */
    public function paginate($data)
    {
        return $this->model
            ->join('projects', 'projects.id', '=', 'properties.project_id')
            ->join('countries', 'countries.id', '=', 'properties.country_id')
            ->join('property_types', 'property_types.id', '=', 'properties.property_type_id')
            ->orderBy($data['currentOrder'], $data['currentOrderBy'])
            ->when($data['search'] && $data['search'] != '', function ($qb) use ($data) {
                return $qb->where('projects.name', 'like', '%' . $data['search'] . '%')
                    ->orWhere('properties.title', 'like', '%' . $data['search'] . '%')
                    ->orWhere('properties.description', 'like', '%' . $data['search'] . '%')
                    ->orWhere('countries.country', 'like', '%' . $data['search'] . '%');
            })
            ->paginate($data['perPage'], ['properties.id', 'properties.title', 'properties.description', 'properties.bathroom',
                'properties.bedroom', 'properties.for_sale',
                'properties.for_rent', 'property_types.type',
                'countries.country', 'projects.name'], 'page', $data['page']);
    }
}
