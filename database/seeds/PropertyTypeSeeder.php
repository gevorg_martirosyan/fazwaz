<?php

use App\Models\PropertyType;
use Illuminate\Database\Seeder;

/**
 * Class PropertyTypeSeeder
 */
class PropertyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['condo', 'house', 'land'];
        foreach ($types as $type){
            PropertyType::firstOrCreate(['type' => $type]);
        }
    }
}
