<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->integer('bedroom')->default(0);
            $table->integer('bathroom')->default(0);
            $table->integer('property_type_id')->unsigned()->index();
            $table->integer('status_id')->unsigned()->index();
            $table->boolean('for_sale')->default(false);
            $table->boolean('for_rent')->default(false);
            $table->integer('project_id')->unsigned()->index();
            $table->integer('country_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
