<?php

use App\Models\Country;
use Illuminate\Database\Seeder;

/**
 * Class CountrySeeder
 */
class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = ['Thailand', 'Cambodia'];
        foreach ($countries as $country){
            Country::firstOrCreate(['country' => $country]);
        }
    }
}
