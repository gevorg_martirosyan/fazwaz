<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class PropertyType
 *
 * @package App\Models
 */
class PropertyType extends Model
{
    /**
     * Get type properties.
     *
     * @return BelongsTo
     */
    public function properties()
    {
        return $this->belongsTo(Property::class);
    }
}
