<html>
<head>
    <title>App Name - @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>

<div class="container">
    <div id="app">
        @yield('content')
    </div>
</div>
<script src="../../js/app.js"></script>
</body>
</html>
