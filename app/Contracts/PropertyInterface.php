<?php


namespace App\Contracts;

/**
 * Interface PropertyInterface
 *
 * @package App\Contracts
 */
interface PropertyInterface
{
    /**
     * Get all projects.
     *
     * @param $data
     * @return mixed
     */
    public function paginate($data);

}
