<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Project
 *
 * @package App\Models
 */
class Project extends Model
{
    /**
     * Get project properties.
     *
     * @return HasMany
     */
    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
