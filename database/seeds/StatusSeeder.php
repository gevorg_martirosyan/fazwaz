<?php

use App\Models\Status;
use Illuminate\Database\Seeder;

/**
 * Class StatusSeeder
 */
class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['active', 'inactive', 'draft'];

        foreach ($types as $type){
            Status::firstOrCreate(['status' => $type]);
        }
    }
}
