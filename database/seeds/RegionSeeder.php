<?php

use App\Models\Country;
use App\Models\Region;
use Illuminate\Database\Seeder;

/**
 * Class RegionSeeder
 */
class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Region::firstOrCreate([
            'region'     => 'Region 1',
            'country_id' => Country::where('country', 'Thailand')->first()->id
        ]);
        Region::firstOrCreate([
            'region'     => 'Region 2',
            'country_id' => Country::where('country', 'Thailand')->first()->id
        ]);
        Region::firstOrCreate([
            'region'     => 'Region 3',
            'country_id' => Country::where('country', 'Cambodia')->first()->id
        ]);
        Region::firstOrCreate([
            'region'     => 'Region 4',
            'country_id' => Country::where('country', 'Cambodia')->first()->id
        ]);
    }
}
