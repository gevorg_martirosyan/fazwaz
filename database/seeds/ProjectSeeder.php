<?php

use App\Models\Project;
use Illuminate\Database\Seeder;

/**
 * Class ProjectSeeder
 */
class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Project::class, 10000)->create();
    }
}
