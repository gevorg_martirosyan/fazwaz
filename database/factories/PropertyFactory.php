<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Country;
use App\Models\Project;
use App\Models\Property;
use App\Models\PropertyType;
use App\Models\Status;
use Faker\Generator as Faker;

$factory->define(Property::class, function (Faker $faker) {
    return [
        'title'            => $faker->text,
        'description'      => $faker->text,
        'bedroom'          => $faker->numberBetween(1, 6),
        'bathroom'         => $faker->numberBetween(1, 3),
        'property_type_id' => PropertyType::all()->random()->id,
        'status_id'        => Status::all()->random()->id,
        'for_sale'         => $faker->boolean,
        'for_rent'         => $faker->boolean,
        'project_id'       => Project::where('id', '!=', 1)->get()->random()->id,
        'country_id'       => Country::whereDoesntHave('regions', function ($qb) {
            $qb->where('region', '=', 'Region 4');
        })->get()->random()->id,
    ];
});
