<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountrySeeder::class);
        $this->call(RegionSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(PropertyTypeSeeder::class);
        $this->call(ProjectSeeder::class);
        $this->call(PropertySeeder::class);
    }
}
