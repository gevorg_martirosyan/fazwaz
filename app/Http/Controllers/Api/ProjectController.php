<?php

namespace App\Http\Controllers\Api;

use App\Contracts\PropertyInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectPaginateRequest;
use Illuminate\Http\JsonResponse;

/**
 * Class ProjectController
 * 
 * @package App\Http\Controllers\Api
 */
class ProjectController extends Controller
{
    /**
     * @var PropertyInterface
     */
    protected $propertyRepo;

    /**
     * ProjectController constructor.
     *
     * @param PropertyInterface $propertyRepo
     */
    public function __construct(PropertyInterface $propertyRepo)
    {
        $this->propertyRepo = $propertyRepo;
    }

    /**
     * Show project data with pagination and sorting.
     *
     * @param ProjectPaginateRequest $request
     * @return JsonResponse
     */
    public function show(ProjectPaginateRequest $request)
    {
        $projects = $this->propertyRepo->paginate($request->inputs());
        return response()->json($projects);
    }
}
