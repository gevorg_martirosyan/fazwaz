<?php

use App\Models\Country;
use App\Models\Project;
use App\Models\Property;
use App\Models\PropertyType;
use App\Models\Status;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

/**
 * Class PropertySeeder
 */
class PropertySeeder extends Seeder
{
    protected $faker;

    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Property::class, 2001)->create([
            'title'            => $this->faker->text,
            'description'      => $this->faker->text,
            'bedroom'          => $this->faker->numberBetween(1, 6),
            'bathroom'         => $this->faker->numberBetween(1, 3),
            'property_type_id' => PropertyType::all()->random()->id,
            'status_id'        => Status::all()->random()->id,
            'for_sale'         => $this->faker->boolean,
            'for_rent'         => $this->faker->boolean,
            'project_id'       => 1,
            'country_id'       => Country::whereDoesntHave('regions', function ($qb) {
                $qb->where('region', '=', 'Region 4');
            })->get()->random()->id,
        ]);
        factory(Property::class, 3000)->create([
            'title'            => $this->faker->text,
            'description'      => $this->faker->text,
            'bedroom'          => 2,
            'bathroom'         => $this->faker->numberBetween(1, 3),
            'property_type_id' => PropertyType::where('type', 'condo')->first()->id,
            'status_id'        => Status::where('status', 'active')->first()->id,
            'for_sale'         => true,
            'for_rent'         => false,
            'project_id'       => Project::where('id', '!=', 1)->get()->random()->id,
            'country_id'       => Country::whereDoesntHave('regions', function ($qb) {
                $qb->where('region', '=', 'Region 4');
            })->get()->random()->id,
        ]);
        factory(Property::class, 94999)->create();
    }
}
