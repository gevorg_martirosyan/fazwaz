<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ProjectPaginateRequest
 *
 * @package App\Http\Requests
 */
class ProjectPaginateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     *  Check and set default values for paginate.
     *
     * @return array
     */
    public function inputs()
    {
        $formData = $this->all();
        if (!isset($formData['currentOrder']) || !$formData['currentOrder']) {
            $formData['currentOrder'] = 'properties.id';
        }
        if (!isset($formData['currentOrderBy']) || !$formData['currentOrderBy']) {
            $formData['currentOrderBy'] = 'asc';
        }
        if (!isset($formData['perPage']) || !$formData['perPage']) {
            $formData['perPage'] = 20;
        }
        if (!isset($formData['page']) || !$formData['page']) {
            $formData['page'] = 1;
        }
        if (!isset($formData['search']) || !$formData['search']) {
            $formData['search'] = null;
        }
        return $formData;
    }
}
