<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Property
 *
 * @package App\Models
 */
class Property extends Model
{
    /**
     * Get project of property.
     *
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get country of property.
     *
     * @return HasOne
     */
    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    /**
     * Get type of property.
     *
     * @return HasOne
     */
    public function propertyType()
    {
        return $this->hasOne(PropertyType::class, 'id', 'property_type_id');
    }

    /**
     * Get status of property.
     *
     * @return HasOne
     */
    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }
}
