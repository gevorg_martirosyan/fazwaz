<?php

namespace Tests\Unit;

use App\Contracts\PropertyInterface;
use App\Country;
use App\Project;
use DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use PropertyTypeSeeder;
use StatusSeeder;
use Tests\TestCase;

class ProjectRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic unit test example.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function test_get_sort_projects()
    {
        factory(Country::class, 1)->create();
        (new DatabaseSeeder())->call(StatusSeeder::class);
        (new DatabaseSeeder())->call(PropertyTypeSeeder::class);
        factory(Project::class, 30)->create();

        $repo = $this->app->make(PropertyInterface::class);

        $response = $repo->getAll('projects.id', 'asc', 'as');

        $this->assertInstanceOf(Project::class, $response[0]);
        $this->assertEquals(20, $response->count());

    }
}
