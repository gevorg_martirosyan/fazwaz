<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Status
 *
 * @package App\Models
 */
class Status extends Model
{
    /**
     * Get status properties.
     *
     * @return BelongsTo
     */
    public function properties()
    {
        return $this->belongsTo(Property::class);
    }
}
